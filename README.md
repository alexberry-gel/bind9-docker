# Bind9 container

A very simple and barely-tested copy of bind9 in a container, with an example gel.zone zone

## Zone file

A simple zone file exists with an A record and a CNAME for gel.zone, it can be seen [here](./conf/var-lib-bind/gel.zone)

## Starting

```
$ docker-compose up -d
```

## Testing

```
$ dig someotherresource.gel.zone @127.0.0.1 -p 1234 +short
someresource.gel.zone.
127.0.0.1

$ dig someresource.gel.zone @127.0.0.1 -p 1234 +short
127.0.0.1
```